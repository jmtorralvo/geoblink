export function routerConfig($stateProvider, $urlRouterProvider) {
    'ngInject';
    $stateProvider
        .state('main', {
            url: '/',
            templateUrl: 'app/sections/main/main.html',
            controller: 'MainController',
            controllerAs: 'main'
        })
        .state('main.table', {
            url: '/table',
            templateUrl: 'app/sections/table/table.html',
            controller: 'TableController',
            controllerAs: 'table',
            resolve: {
                model: (DataHandler) => {
                    return DataHandler.getData();
                }
            }
        })
        .state('main.graph', {
            url: '/graph',
            templateUrl: 'app/sections/graph/graph.html',
            controller: 'GraphController',
            controllerAs: 'graph',
            resolve: {
                model: (DataHandler) => {
                    return DataHandler.getData();
                }
            }
        });

    $urlRouterProvider.otherwise('/');
}
