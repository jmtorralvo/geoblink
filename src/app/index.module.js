/* global malarkey:false, moment:false */

import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { MainController } from './sections/main/main.controller';
import { TableController } from './sections/table/table.controller';
import { GraphController } from './sections/graph/graph.controller';
import { ApiHandler } from '../app/services/apiHandler/apiHandler.service';
import { DataHandler } from '../app/services/dataHandler/dataHandler.service';
import { NavbarDirective } from '../app/directives/navbar/navbar.directive';
import { MalarkeyDirective } from '../app/directives/malarkey/malarkey.directive';

angular.module('test1', ['ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'ui.bootstrap', 'toastr', 'firebase', 'nvd3', 'chart.js'])
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .config(config)
    .config(routerConfig)
    .run(runBlock)
    .service('apiHandler', ApiHandler)
    .service('DataHandler', DataHandler)
    .controller('MainController', MainController)
    .controller('TableController', TableController)
    .controller('GraphController', GraphController)
    .directive('geoblinkNavbar', NavbarDirective)
    .directive('acmeMalarkey', MalarkeyDirective);
