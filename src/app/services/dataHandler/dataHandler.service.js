export class DataHandler {
    constructor() {
        'ngInject';
        this.model = {};
    }

    fillData(obj) {
        this.model = obj.inputs;
    }

    getData(){
        return this.model;
    }
}
