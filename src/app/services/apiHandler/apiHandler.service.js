
export class ApiHandler {
  constructor ($resource) {
    'ngInject';
    // toDo: Node app running behind, no Mocks
    return $resource('app/mocks/dataGeo'); // Note the full endpoint address
  }
}
