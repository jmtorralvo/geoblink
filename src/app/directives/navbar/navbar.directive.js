export function NavbarDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'app/directives/navbar/navbar.html',
    scope: {
        creationDate: '='
    },
    controller: NavbarController,
    controllerAs: 'navbar',
    bindToController: true
  };

  return directive;
}

class NavbarController {
  constructor (moment) {
    'ngInject';

    // "this.creation" is available by directive option "bindToController: true"
    const vm = this;
    vm.logo = "Foo Logo";
    this.relativeDate = moment(this.creationDate).fromNow();
  }
}
