/**
 * @todo Complete the test
 * This example is not perfect.
 * Test should check if MomentJS have been called
 */
describe('directive navbar', function() {
  let vm,
    element,
    timeInMs,
    scope,
    testCont;

  beforeEach(angular.mock.module('test1'));

  beforeEach(inject(($compile, $rootScope) => {
    const currentDate = new Date();
    timeInMs = currentDate.setHours(currentDate.getHours() - 24);

    element = angular.element(`
      <geoblink-navbar creation-date="${timeInMs}"></geoblink-navbar>
    `);
 
    $compile(element)($rootScope.$new());
    $rootScope.$digest();
    vm = element.isolateScope().navbar;
  }));

  it('should be compiled', () => {
    expect(element.html()).not.toEqual(null);
  });

  it('should have isolate scope object with instanciate members', () => {

    expect(vm).toEqual(jasmine.any(Object));

    expect(vm.creationDate).toEqual(jasmine.any(Number));
    expect(vm.creationDate).toEqual(timeInMs);

    expect(vm.relativeDate).toEqual(jasmine.any(String));
    expect(vm.relativeDate).toEqual('a day ago');
  });
});
