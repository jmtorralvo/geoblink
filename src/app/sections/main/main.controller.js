/*global Firebase*/
/*eslint-disable no-unused-vars*/

export class MainController {
    constructor($firebaseObject, toastr, $scope, DataHandler, $state) {
        'ngInject';

        const vm = this;
        vm.user = 'Jose Manuel Torralvo Moyano';
        vm.fooData = [];
        vm.creationDate = new Date().getTime();
        $scope.id = 3;

        let ref = new Firebase('https://scorching-torch-7081.firebaseio.com/geoblink');
        let syncObject = $firebaseObject(ref);

        syncObject.$bindTo($scope, "data")
            .then((data) => {
                toastr.success('Conectado con la base de datos');
                DataHandler.fillData($scope.data);
                $state.go('main.graph');
                //console.log(DataHandler.getData('table'));
            }, (error) => {
                toastr.error('No hemos podido contactar con la base de datos');
            });
    }
}
