export class GraphController {
    constructor(model) {
        'ngInject';

        const vm = this;
        vm.componentName = "Graph";
        vm.model = model;
        vm.chartLabel = [];
        vm.chartData = [];
        vm.colours = [{
            fillColor: 'rgba(217, 37, 36, 0.6)',
            strokeColor: 'rgba(217, 37, 36,0.9)',
            highlightFill: 'rgba(217, 37, 36, 0.8)',
            highlightStroke: 'rgba(217, 37, 36, 1)'
        }, {
            fillColor: 'rgba(235, 128, 0, 0.6)',
            strokeColor: 'rgba(235, 128, 0, 0.9)',
            highlightFill: 'rgba(235, 128, 0, 0.8)',
            highlightStroke: 'rgba(235, 128, 0, 1)'
        }, {
            fillColor: 'rgba(21, 149, 162, 0.6)',
            strokeColor: 'rgba(21, 149, 162, 0.9)',
            highlightFill: 'rgba(21, 149, 162, 0.8)',
            highlightStroke: 'rgba(21, 149, 162, 1)'
        }];
        vm.addresses = [];
        vm.chartOptions = {
            //Boolean - Whether to show lines for each scale point
            scaleShowLine: true,
            //Boolean - Whether we show the angle lines out of the radar
            angleShowLineOut: true,
            //Boolean - Whether to show labels on the scale
            scaleShowLabels: false,
            // Boolean - Whether the scale should begin at zero
            scaleBeginAtZero: true,
            //String - Colour of the angle line
            angleLineColor: "rgba(0,255,0,.2)",
            //Number - Pixel width of the angle line
            angleLineWidth: 1,
            //String - Point label font declaration
            pointLabelFontFamily: "'helvetica'",
            //String - Point label font weight
            pointLabelFontStyle: "normal",
            //Number - Point label font size in pixels
            pointLabelFontSize: 10,
            //String - Point label font colour
            pointLabelFontColor: "#666",
            //Boolean - Whether to show a dot for each point
            pointDot: true,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 3,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a colour
            datasetFill: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
        };

        this.transformJSON(vm.model);
    }

    transformJSON(mod) {
        /// Labels
        angular.forEach(mod[0].variables.indexes, (value, key) => {
            this.chartLabel.push(key);
        });

        angular.forEach(mod, (value) => {
            this.addresses.push(value.address);
            let tempArray = []
            angular.forEach(value.variables.indexes, (value) => {
                tempArray.push(value);
            });
            this.chartData.push(tempArray);
        });
    }
}
