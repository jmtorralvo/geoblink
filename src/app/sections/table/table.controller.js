export class TableController {
    constructor(model) {
        'ngInject';

        const vm = this;
        const coloursClasses = ['red-label', 'yellow-label', 'blue-label'];
        //Expose properties
        vm.componentName = "Table";
        vm.model = model;

        //Expose Method
        vm.getClass = getClass;

        this.transformJSON();

        function getClass(index) {
            return coloursClasses[index];
        }
    }

    transformJSON() {
        for (var i = 0; i < this.model.length; i++) {
            this.model[i].title = (i === 0) ? 'Reference Area' : ('Compared Area '+ i);
        }
    }
}
